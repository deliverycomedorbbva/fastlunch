//Variables para el módulo de seguridad JWT
var jwt = require('jsonwebtoken');
const PTOKEN = process.env.PASS_TOKEN;

//Funcion para JWT
function isAuth(req, res,next){
    var token = req.headers['authorization']
    if(!token){
        res.status(401).send({
          error: "Es necesario el token de autenticación"
        })
        return
    }
    token = token.replace('Bearer ', '')
    jwt.verify(token, PTOKEN, function(err, user) {
      if (err) {
        res.status(401).send({
          error: 'Token inválido'
        })
      } else {
        next();
      }
    })
}

module.exports={isAuth};
