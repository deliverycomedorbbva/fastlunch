var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var tarjetaSchema = new Schema(
  {
    tarjetaId         : { type: Number, required: true, index: { unique: true },trim: true,key: true},
    numero            : { type: String, required: true, trim: true},
    nombre            : { type: String, required: true, trim: true},
    fecha             : { type: String, required: true, trim: true} 
  }
);

var userSchema = new Schema({
      idUsuario       : { type: Number, required: true, index: { unique: true },trim: true,key: true},
      nombres         : { type: String, required: true, trim: true},
      apellidos       : { type: String, required: true, trim: true},
      correo          : { type: String, required: true, trim: true},
      password        : { type: String, required: true, trim: true},
      tarjeta         : [tarjetaSchema],
      telefono        : { type: Number, required: true, trim: true, min: 9, max: 9},
      estado          : { type: String, required: true, trim: true, min: 1, max: 1},
      nombreImg       : { type: String, required: true, trim: true}
      });

module.exports = userSchema ;
