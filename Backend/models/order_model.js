var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var productSchema = new Schema(
  {
    idProducto        : { type: Number, required: true, index: { unique: true },trim: true,key: true,
                          ref: 'product'},
    cantidad          : { type: Number, required: true, trim: true}
  }
);

var orderSchema = new Schema({
      idPedido        : { type: Number, required: true, index: { unique: true }, trim: true, key: true},
      producto        : [productSchema],
      idUsuario       : { type: Number, required: true, index: { unique: true }, trim: true,
                          ref : 'user', key: true},
      montoTotal      : { type: Number, required: true, trim: true},
      fechaRegistro   : { type: String, required: true, trim: true},
      fechaEntrega    : { type: String, required: true, trim: true},
      fechaRecojo     : { type: String, required: true, trim: true},
      fechaCancelado  : { type: String, required: true, trim: true},
      horarioAsignado : { type: String, required: true, trim: true, min: 2, max: 2,
                          ref : 'fastLunchConfig', key: true,index: { unique: true }},
      tipoPago        : { type: String, required: true, trim: true, min: 2, max: 2},
      estado          : { type: String, required: true, trim: true}
    },{ toJSON: { virtuals: true } ,
        toObject: { virtuals: true} },{ autoIndex: false });


const order = mongoose.model('order', orderSchema);

   orderSchema.virtual('orderProds', {
     ref: 'product', // The model to use
     localField: 'producto.idProducto', // Find people where `localField`
     foreignField: 'idProducto'
   });

   orderSchema.virtual('detalleHorario', {
     ref: 'fastLunchConfig', // The model to use
     localField: 'horarioAsignado', // Find people where `localField`
     foreignField: 'horarioAsignado'
   });


module.exports = orderSchema;
