var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var productSchema = new Schema({
      idProducto       : { type: Number, required: true, index: { unique: true } ,trim: true, key: true},
      nombre           : { type: String, required: true, trim: true},
      descripcion      : { type: String, required: true, trim: true},
      stock            : { type: Number, required: true, trim: true},
      nombreImg        : { type: String, required: true, trim: true},
      typeId           : { type: Number, required: true, index: { unique: true } ,trim: true,
                           ref: 'productType', key: true},
      indDia           : { type: String, required: true, trim: true, min: 1, max: 1},
      precio           : { type: Number, required: true, trim: true}
    },{ toJSON: { virtuals: true },toObject: { virtuals: true} });

module.exports = productSchema ;
