var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var productTypeSchema = new Schema({
      typeId           : { type: Number, required: true, index: { unique: true } ,trim: true, key: true},
      name             : { type: String, required: true, trim: true},
      principalMessage : { type: String, required: true, trim: true},
      secondMessage    : { type: String, required: true, trim: true},
      imgUrl           : { type: String, required: true, trim: true},
      welcomeMessage   : { type: String, required: true, trim: true}
      });

module.exports = productTypesSchema ;
