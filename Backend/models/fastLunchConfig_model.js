var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var fastLunchConfigSchema = new Schema({
      idConfig         : { type: Number, required: true, index: { unique: true } ,trim: true, key: true},
      horarioAsignado  : { type: String, required: true, trim: true, key: true,index: { unique: true }},
      description      : { type: String, required: true, trim: true},
      maxTime          : { type: String, required: true, trim: true},
      maxDaysToOrder   : { type: Number, required: true, trim: true},
      maxTimeDay       : { type: String, required: true, trim: true}
    },{ toJSON: { virtuals: true } ,toObject: { virtuals: true} });

module.exports = fastLunchConfigSchema;
