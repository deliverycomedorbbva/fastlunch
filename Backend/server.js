// autor: GRUPO 4 BACK-END
// fecha: 14/08/19
// elemento:server.js - FAST LUNCH

//VARIABLES DE ENTORNO Y GLOBALES
var express = require('express');
var app =  express();
const cors = require('cors');
require('dotenv').config();
var bodyParser = require('body-parser');
const URL_BASE = '/deliveryComedor/v1/';
const PORT = process.env.PORT;
let requestJSON = require('request-json');
var queryStringField = 'f={"_id":0}&';

//CONEXION MLAB WITH MONGOOSE
var mongoose = require('mongoose');
const URI = process.env.MLAB_URI;
const OPTIONS = {
   useNewUrlParser: true,
   useCreateIndex: true,
   useUnifiedTopology: true,
   connectTimeoutMS: 30000, // Give up initial connection after 30 seconds
   socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
}

mongoose.connect(URI, OPTIONS).catch(error => handleError(error));
var conn = mongoose.connection;
conn.on('error', console.error.bind(console, 'connection error:'));
conn.once('open',function(err,res){
    console.log('connected to mongodb mlab')
  });
//
//REFERENCIAS A CONTROLADORES
const user_controller = require('./controllers/user_controller');
const product_controller = require('./controllers/product_controller');
const order_controller = require('./controllers/order_controller');
const productType_controller = require('./controllers/productType_controller');
const fastLunchConfig_controller = require('./controllers/fastLunchConfig_controller');

//REFERENCIAS PARA USAR TOKEN
const seg=require('./middlewares/auth');
//
app.use(cors());
app.options('*',cors());
app.use(bodyParser.json());

//<<<OPERACIONES CRUD COLECCION PRODUCTO>>>
  //GET todos los productos
  app.get(URL_BASE+'products',seg.isAuth,product_controller.getProduct);
  app.get(URL_BASE+'products/:id',seg.isAuth,product_controller.getProductById);
  app.get(URL_BASE+'products/type/:typeId',seg.isAuth,product_controller.getProductByType);
  app.post(URL_BASE + 'products',seg.isAuth,product_controller.createProduct);
  app.put(URL_BASE + 'products/:id',seg.isAuth,product_controller.updateProductById);
  app.delete(URL_BASE + "products/:id",seg.isAuth,product_controller.deleteProductById);
//<<< >>>

//<<<OPERACIONES CRUD COLECCION USUARIO>>>
  app.get(URL_BASE + 'users',seg.isAuth,user_controller.getUsers);
  app.get(URL_BASE + 'users/:id',seg.isAuth,user_controller.getUsersId);
  app.get(URL_BASE + 'users/cards/:id',seg.isAuth,user_controller.getCards);
  app.post(URL_BASE + 'users/login',user_controller.login);
  app.post(URL_BASE + 'users',user_controller.createUser);
  app.put(URL_BASE + 'users/:id',seg.isAuth,user_controller.updateUserById);
  app.delete(URL_BASE + 'users/:id',seg.isAuth,user_controller.deleteUserById);
//<<< >>>

//<<<OPERACIONES CRUD COLECCION PEDIDOS>>>
  app.get(URL_BASE + 'orders',seg.isAuth,order_controller.getOrder);
  app.get(URL_BASE + 'orders/:id',seg.isAuth,order_controller.getOrderById);
  app.get(URL_BASE + 'orders/user/:idUser',seg.isAuth,order_controller.getOrderByUser);
  app.get(URL_BASE + 'orders/date/:date',seg.isAuth,order_controller.getOrderByDate);
  app.get(URL_BASE + 'orders/user/date/:idUser&:date',seg.isAuth,order_controller.getOrderByUserDate);
  app.post(URL_BASE + 'orders',seg.isAuth,order_controller.createOrder);
  app.put(URL_BASE + 'orders/estado/:status&:id&:date',seg.isAuth,order_controller.updateOrderEstadoFecha);
  app.delete(URL_BASE + "orders/:id",seg.isAuth,order_controller.deleteOrderById);
//<<< >>>

//<<<OPERACIONES CRUD COLECCION TIPO DE PRODUCTO>>>
app.get(URL_BASE + 'productTypes',seg.isAuth,productType_controller.getProductTypes);
app.get(URL_BASE + 'productTypes/:id',seg.isAuth,productType_controller.getProductTypeById);
app.post(URL_BASE + 'productTypes',seg.isAuth,productType_controller.createProductType);
app.put(URL_BASE + 'productTypes/:id',seg.isAuth,productType_controller.updateProductTypeById);
app.delete(URL_BASE + 'productTypes/:id',seg.isAuth,productType_controller.deleteProductTypeById);
//<<< >>>

//<<<OPERACIONES CRUD COLECCION CONFIG FASTLUNCH>>>
app.get(URL_BASE + 'fastLunchConfigs',seg.isAuth,fastLunchConfig_controller.getConfigById);
app.put(URL_BASE + 'fastLunchConfigs/:id',seg.isAuth,fastLunchConfig_controller.updateConfigById);
//<<< >>>

app.listen(PORT,function()
  {console.log('Api escuchando en puerto 3000');});
