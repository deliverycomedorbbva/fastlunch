//Variables y constantes globales
var requestJson = require('request-json');
const apiKeyMLab="apiKey="+process.env.MLAB_API_KEY;
const PTOKEN = process.env.PASS_TOKEN;
const baseMLabURL=process.env.BASEMLABURL;
var qStringField = 'f={"_id":0,"password":0}&';
var limFlt = 'l=1&';
const BCRYPT = require("bcryptjs");
const BCRYPT_SALT_ROUNDS=process.env.BCRYPT_SALT_ROUNDS;
var httpClient = requestJson.createClient(baseMLabURL);
const CRYPTR = require('cryptr');
const KEY_WORD = process.env.KEY_WORD_BASE;
const cryptr = new CRYPTR(KEY_WORD);

var jwt = require('jsonwebtoken');
var bodyParser = require('body-parser');
var express = require('express');
var app =  express();

//Inicio Token
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json({limit:'10mb'}));
//Fin Token

//GET Listar todos los usuarios
function getUsers(req,res){
    httpClient.get('users?' + qStringField + apiKeyMLab,
      function(err, respuestaMLab, body){
        if (err) {
          res.status(500).send({"msg" : "Error obteniendo usuario"});
        }else {
          if (body.length > 0) {
            res.status(201).send(body);
          }else {
            res.status(404).send({"msg" : "Ningún elemento usuario"});
            ;}}
      })
}
module.exports.getUsers = getUsers;

//Login usuario
function login(req,res){
    let correo = req.body.correo;
    let pass = req.body.password;
    let queryString = 'q={"correo":"' + correo + '"}&';
    httpClient.get('users?' + queryString + limFlt + apiKeyMLab,
      function(err,respuestaMLab,body){
        if (!err) {
          if (body.length == 1)
          {
                var re={};
                var nstatus=0;
                BCRYPT.compare(pass, body[0].password, function(err, info) {
                  if(info) {
                   // Passwords match
                     var tokenData ={username: correo}
                     var token = jwt.sign(tokenData, PTOKEN, {expiresIn: 60 * 60 * 24 }) //24 horas expira
                     //datos de tarjeta en claro
                     var tarjetaArray = body[0].tarjeta;
                     var newTarjetaArray = [];
                     var nroTarjetaDecrypt;
                     //encripto nro de tarjeta usuario para guardar en mongodb
                     for(var x=0;x < tarjetaArray.length;x++)
                     {
                       nroTarjetaDecrypt =  cryptr.decrypt(tarjetaArray[x].numero);
                       tarjetaArray[x].numero = nroTarjetaDecrypt;
                       newTarjetaArray.push(tarjetaArray[x]);
                     }
                     body.tarjeta = newTarjetaArray;
                     //datos de usuario:
                     var usuario = {
                        "idUsuario"  : body[0].idUsuario,
                        "nombres"    : body[0].nombres,
                        "apellidos"  : body[0].apellidos,
                            "correo" : body[0].correo,
                         "tarjeta"   : body.tarjeta,
                         "telefono"  : body[0].telefono,
                         "estado"    : body[0].estado,
                         "nombreImg" : body[0].nombreImg
                       };
                      res.status(200).send({usuario, token});
                    } else {
                     // Passwords don't match
                     res.status(404).send({"error":"Usuario o password no validos"});
                    }
                  });

          } else {
            res.status(404).send({"error":"Usuario o password no validos"});
          }
        } else {
          res.status(511).send({"error": "Error en servidor"});
        }
       }
    )
  }
module.exports.login = login;

//GET Mostrar un usuario
function getUsersId(req,res){
  var id = req.params.id;
  var qStringId = 'q={"idUsuario":' + id + '}&';
  httpClient.get('users?' + qStringId + qStringField + apiKeyMLab,
    function(err, respuestaMLab, body){
      var response = {};
      if (err) {
        res.status(500).send({"error" : "Error obteniendo usuario"});
      }else {
        if (body.length > 0) {
          var tarjetaArray = body[0].tarjeta;
          var newTarjetaArray = [];
          var nroTarjetaDecrypt;
          //encripto nro de tarjeta usuario para guardar en mongodb
          for(var x=0;x < tarjetaArray.length;x++)
          {
            nroTarjetaDecrypt =  cryptr.decrypt(tarjetaArray[x].numero);
            tarjetaArray[x].numero = nroTarjetaDecrypt;
            newTarjetaArray.push(tarjetaArray[x]);
          }
          body.tarjeta = newTarjetaArray;
          res.status(201).send(body);
        }else {
          res.status(404).send({"error" : "Ningún usuario"});
        }
      }
    });
}
module.exports.getUsersId = getUsersId;

//GET Mostrar tarjetas de un usuario
  function getCards(req,res){
    var id = req.params.id;
    var qStringId = 'q={"idUsuario":' + id + '}&';
    var qFilter = 'f={"tarjeta":1,"_id":0}&';
    httpClient.get('users?' + qStringId + qFilter + apiKeyMLab,
      function(err, respuestaMLab, body){
        if (err) {
          res.status(500).send({"error" : "Error obteniendo tarjetas de usuario"});
        }else {
          if (body.length > 0) {
            var tarjetaArray = body[0].tarjeta;
            var newTarjetaArray = [];
            var nroTarjetaDecrypt;
            //encripto nro de tarjeta usuario para guardar en mongodb
            for(var x=0;x < tarjetaArray.length;x++)
            {
              nroTarjetaDecrypt =  cryptr.decrypt(tarjetaArray[x].numero);
              tarjetaArray[x].numero = nroTarjetaDecrypt;
              newTarjetaArray.push(tarjetaArray[x]);
            }
            //body.tarjeta = newTarjetaArray;
            res.status(201).send(newTarjetaArray);
          }else {
            res.status(404).send({"error" : "Usuario no tiene tarjetas'"});
          }
        }
      })
  }
  module.exports.getCards = getCards;

//Alta usuario, valida correo (campo obligatorio en front)
  function createUser(req, res){
    var correo =  req.body.correo;
    var queryString = 'q={ "correo":"'+correo+'"}&';
    httpClient.get('users?' + queryString + apiKeyMLab,
      function(err, respuestaMLab, body){
        let response = body[0];
        if (typeof response == "undefined"){
          var queryString = 's={ idUsuario: -1}&';
          httpClient.get('users?' + queryString + apiKeyMLab,
            function(error, respuestaMLab, body){
                  var ulId = parseInt(body[0].idUsuario);
                  var encrypPass = "";
                //encriptar contraseña
                  BCRYPT.hash(req.body.password, BCRYPT_SALT_ROUNDS,
                    function (err, hash) {
                        if(error)
                        {
                          res.status(500).send({"error":"Error al generar contraseña"});
                        }
                        else {
                          encrypPass = hash;
                        }
                      });
                  var tarjetaArray = req.body.tarjeta;
                  var newTarjetaArray = [];
                  var nroTarjetaEncryp;
                  //encripto nro de tarjeta usuario para guardar en mongodb
                  for(var x=0;x < tarjetaArray.length;x++)
                  {
                    nroTarjetaEncryp =  cryptr.encrypt(tarjetaArray[x].numero);
                    tarjetaArray[x].numero = nroTarjetaEncryp;
                    newTarjetaArray.push(tarjetaArray[x]);
                  }

                  newID = ulId + 1;
                  var newUsuario = {
                     "idUsuario"  : newID,
                     "nombres"    : req.body.nombres,
                     "apellidos"  : req.body.apellidos,
                         "correo" : req.body.correo,
                     "password"   : encrypPass,
                      "tarjeta"   : newTarjetaArray,
                      "telefono"  : req.body.telefono,
                      "estado"    : req.body.estado,
                      "nombreImg" : req.body.nombreImg
                  };
                    httpClient.post(baseMLabURL + "users?" +  apiKeyMLab, newUsuario,
                    function(error, respuestaMLab, body){
                      if(error)
                        {
                          res.status(500).send({"error":"Error al crear usuario"});
                        }
                        else {
                          res.status(201).send({"msg":"Usuario creado"});
                        }
                    });
            });
          }
          else {
            res.status(302).send({"error":"Usuario ya registrado "+req.body.correo});
          }
        })
  };
module.exports.createUser = createUser;

//ACTUALIZAR usuario por ID
  function updateUserById(req, res) {
      var id = req.params.id;
      var nid = parseInt(id);
      var queryString = 'q={"idUsuario":' + id + '}&';
      httpClient.get('users?' + queryString + apiKeyMLab,
        function(err, respuestaMLab, body){
          let response = body[0];
          if (typeof response !== "undefined"){
             var encrypPass = "";
             //encriptar contraseña
               BCRYPT.hash(req.body.password, BCRYPT_SALT_ROUNDS,
                 function (err, hash) {
                     if(error)
                     {
                       res.status(500).send({"error":"Error al generar contraseña"});
                     }
                     else {
                       encrypPass = hash;
                     }
                   });

             var tarjetaArray = req.body.tarjeta;
             var newTarjetaArray = [];
             var nroTarjetaEncryp;
             //encripto nro de tarjeta usuario para guardar en mongodb
             for(var x=0;x < tarjetaArray.length;x++)
             {
               nroTarjetaEncryp =  cryptr.encrypt(tarjetaArray[x].numero);
               tarjetaArray[x].numero = nroTarjetaEncryp;
               newTarjetaArray.push(tarjetaArray[x]);
             }

              let updatedUser = {
                "idUsuario"  : nid,
                "nombres"    : req.body.nombres,
                "apellidos"  : req.body.apellidos,
                    "correo" : req.body.correo,
                  "password" : encrypPass,
                 "tarjeta"   : newTarjetaArray,
                 "telefono"  : req.body.telefono,
                 "estado"    : req.body.estado,
                 "nombreImg" : req.body.nombreImg
              };
              httpClient.put('products/' + response._id.$oid + '?' + apiKeyMLab, updatedUser,
                function(err, respuestaMLab, body){
                  var response = {};
                  if(err) {
                      res.status(500).send({"error" : "Error actualizando usuario."});
                  } else {
                      res.status(200).send({ "msg" : "usuario actualizado correctamente."});
                    }
                });
            }
            else {
              res.status(500).send({"error" : "Usuario no existe para actualizar."});
            }
        });
  };
module.exports.updateUserById = updateUserById;

//DELETE producto por ID
  function deleteUserById(req, res){
    var id = req.params.id;
    var queryStringID = 'q={"idUsuario":' + id + '}&';
    httpClient.get('users?' +  queryStringID + apiKeyMLab,
      function(error, respuestaMLab, body){
        var respuesta = body[0];
        if (typeof respuesta !== "undefined"){
            httpClient.delete(baseMLabURL + "users/" + respuesta._id.$oid +'?'+ apiKeyMLab,
            function(error, respuestaMLab,body){
                  res.status(200).send({"msg": "usuario "+body[0].correo+" ha sido eliminado"});
                });
            }
        else {
            res.status(404).send({"error": "no existe usuario a eliminar."});
           }
         });
   };
module.exports.deleteUserById = deleteUserById;
