const apiKeyMLab="apiKey="+process.env.MLAB_API_KEY;
const baseMLabURL=process.env.BASEMLABURL;
var express = require('express');
const cors = require('cors');
let requestJSON = require('request-json');
var queryStringField = 'f={"_id":0}&';
var httpClient = requestJSON.createClient(baseMLabURL);

//GET todos los tipos de productos definidos
function getProductTypes(req,res){
          httpClient.get('productTypes?'+queryStringField+apiKeyMLab,
          function(err, respuestaMLab, body)
          {
            if(err){
              res.status(500).send({"msg":"error obteniendo tipos de producto."});
            }
            else {
                if (body.length>0) {
                  res.status(200).send(body);
                }
                else {
                  res.status(404).send({"msg":"ningun elemento en tipos de producto."});
                }
            }
          });
  };

//GET tipos producto por ID
function getProductTypeById(req,res){
      var id=req.params.id;
      var queryString = 'q={"typeId":'+id+'}&';
      httpClient.get('productTypes?'+queryString+queryStringField+apiKeyMLab,
      function(err, respuestaMLab, body)
        {
          if(err){
              res.status(500).send({"msg":"error obteniendo tipo de producto."});
            }
          else {
                if (body.length>0) {
                    res.status(200).send(body);
                  }
                else {
                  res.status(404).send({"msg":"ningun elemento tipo de producto."});
                }
          }
      });
};

//INSERTAR tipo de producto
 function createProductType(req, res){
         var queryString = 's={ typeId: -1}&';
         httpClient.get('productTypes?' + queryString + apiKeyMLab,
           function(error, respuestaMLab, body){
                 var ulId = parseInt(body[0].typeId);
                 newID = ulId + 1;
                 var newProductType = {
                    "typeId"    : newID,
                    "name"      : req.body.name,
                    "principalMessage" : req.body.principalMessage,
                    "secondMessage"    : req.body.secondMessage,
                     "imgUrl"          : req.body.imgUrl,
                     "welcomeMessage"  : req.body.welcomeMessage
                 };
                 httpClient.post(baseMLabURL + "productTypes?" +  apiKeyMLab, newProductType,
                   function(error, respuestaMLab, body){
                     res.status(201).send(body);
                   });
           });
};

//ACTUALIZAR producto por ID
function updateProductTypeById(req, res) {
     var id = req.params.id;
     var nid = parseInt(id);
     var queryString = 'q={"typeId":' + id + '}&';
     httpClient.get('productTypes?' + queryString + apiKeyMLab,
       function(err, respuestaMLab, body){
         let response = body[0];
         if (typeof response !== "undefined"){
             //Actualizo campos del producto
             let updatedProductType = {
               "typeId"  : nid,
               "name"      : req.body.name,
               "principalMessage" : req.body.principalMessage,
               "secondMessage"    : req.body.secondMessage,
                "imgUrl"          : req.body.imgUrl,
                "welcomeMessage"  : req.body.welcomeMessage
             };
             httpClient.put('productTypes/' + response._id.$oid + '?' + apiKeyMLab, updatedProductType,
               function(err, respuestaMLab, body){
                 if(err) {
                     res.status(500).send({"msg" : "Error actualizando tipo de producto."});
                 } else {
                     res.status(200).send({"msg" : "Tipo de producto actualizado correctamente."});
                 }
               });
           }
           else {
             res.status(500).send({"msg" : "Tipo de Producto no existe para actualizar."});
           }
       });
 };

 //DELETE producto por ID
  function deleteProductTypeById(req, res){
    var id = req.params.id;
    var queryStringID = 'q={"typeId":' + id + '}&';
    httpClient.get('productTypes?' +  queryStringID + apiKeyMLab,
      function(error, respuestaMLab, body){
        var respuesta = body[0];
        if (typeof respuesta !== "undefined"){
            httpClient.delete(baseMLabURL + "productTypes/" + respuesta._id.$oid +'?'+ apiKeyMLab,
              function(error, respuestaMLab,body){
                  res.status(200).send(body);
                })
            }
        else {
            res.status(500).send({"error": "no existe tipo de producto a eliminar."});
           }
         });
   };

module.exports.getProductTypes = getProductTypes;
module.exports.getProductTypeById = getProductTypeById;
module.exports.createProductType = createProductType;
module.exports.updateProductTypeById = updateProductTypeById;
module.exports.deleteProductTypeById = deleteProductTypeById;
