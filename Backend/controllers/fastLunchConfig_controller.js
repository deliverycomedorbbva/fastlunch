const apiKeyMLab="apiKey="+process.env.MLAB_API_KEY;
const baseMLabURL=process.env.BASEMLABURL;
var express = require('express');
const cors = require('cors');
let requestJSON = require('request-json');
var queryStringField = 'f={"_id":0}&';
var httpClient = requestJSON.createClient(baseMLabURL);

//GET tipos producto por ID
function getConfigById(req,res){
      httpClient.get('fastLunchConfigs?'+queryStringField+apiKeyMLab,
      function(err, respuestaMLab, body)
        {
          if(err){
              res.status(500).send({"Error":"error obteniendo configuracion FastLuch."});
            }
          else {
                if (body.length>0) {
                    res.status(200).send(body);
                  }
                else {
                  res.status(404).send({"msg":"ningun elemento configuracion FastLuch."});
                }
          }
      });
};

//ACTUALIZAR producto por ID
function updateConfigById(req, res) {
     var id = req.params.id;
     var nid = parseInt(id);
     var queryString = 'q={"idConfig":' + id + '}&';
     httpClient.get('fastLunchConfigs?' + queryString + apiKeyMLab,
       function(err, respuestaMLab, body){
         let response = body[0];
         if (typeof response !== "undefined"){
             //Actualizo campos del producto
             let updatedConfig = {
               "idConfig"         : nid,
               "horarioAsignado"  : req.body.horarioAsignado,
               "description    "  : req.body.description,
               "maxTime"          : req.body.maxTime,
               "maxDaysToOrder"   : req.body.maxDaysToOrder,
               "maxTimeDay"       : req.body.maxTimeDay
             };
             httpClient.put('fastLunchConfigs/' + response._id.$oid + '?' + apiKeyMLab, updatedConfig,
               function(err, respuestaMLab, body){
                 if(err) {
                     res.status(500).send({"Error" : "Error actualizando configuracion FastLuch."});
                 } else {
                     res.status(200).send({"msg" : "Configuracion FastLuch actualizado correctamente."});
                 }
               });
           }
           else {
             res.status(500).send({"Error" : "Tipo de Producto no existe para actualizar."});
           }
       });
 };

module.exports.getConfigById = getConfigById;
module.exports.updateConfigById = updateConfigById;
