const apiKeyMLab="apiKey="+process.env.MLAB_API_KEY;
const baseMLabURL=process.env.BASEMLABURL;
const nodemailer = require('nodemailer');
const express = require('express');
const FS = require("fs")
const cors = require('cors');
const requestJSON = require('request-json');
const QRCode = require('qrcode')
const queryStringField = 'f={"_id":0}&';
const queryOrder = 's={ idPedido: -1}&'
const limFlt = 'l=1&';
const qStringField = 'f={"_id":0,"password":0}&';

//MONGOOSE
const  orderSchema  = require('../models/order_model.js');
const  userSchema  = require('../models/user_model.js');
const  productSchema  = require('../models/product_model.js');
const  fastLunchConfigSchema  = require('../models/fastLunchConfig_model.js');

//var mongoose = require('mongoose');
const mongoose = require('mongoose');
const order = mongoose.model('order', orderSchema,'orders');
const product = mongoose.model('product', productSchema,'products');
const user = mongoose.model('user', userSchema,'users');
const fastLunchConfig = mongoose.model('fastLunchConfig', fastLunchConfigSchema,'fastLunchConfigs');
//
var httpClient = requestJSON.createClient(baseMLabURL);
//Variables para envío de correos
// Definimos el transporter
var mensaje;
var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: process.env.USER_EMAIL,
            pass: process.env.PASS_EMAIL
        }
      });
///<<funciones>>>>
//GET todos los Pedidos
function getOrder(req,res){
  order.find({})
   .populate({path: 'orderProds', select:'nombre descripcion nombreImg typeId precio -_id'})
   .populate({path: 'detalleHorario', select: 'description maxTime maxTimeDay -_id'})
   .select({ "_id":0,"id":0})
   .sort({idPedido : -1})
    .exec(function(error, resultado) {
        if (resultado.length<=0)
        {
        res.status(404).send({"error" : "Pedido no existe" });
        }
        else {
          res.status(201).send(resultado);
        }
      });
  };

//GET Pedido por ID join productos
  function getOrderById (req, res){
      var idPed = Number(req.params.id);
      order.find({"idPedido":idPed})
       .populate({path: 'orderProds', select:'nombre descripcion nombreImg typeId precio -_id'})
       .populate({path: 'detalleHorario', select: 'description maxTime maxTimeDay -_id'})
       .select({ "_id":0,"id":0})
        .exec(function(error, resultado) {
            if (resultado.length<=0)
            {
            res.status(404).send({"error" : "Pedido no existe" });
            }
            else {
              res.status(201).send(resultado);
            }
          });
 };

//GET Pedido por Usuario 'orderProds'
function getOrderByUser(req,res){
        var idUser = req.params.idUser;
        order.find({"idUsuario":idUser})
        .populate({path: 'orderProds', select:'nombre descripcion nombreImg typeId precio -_id'})
        .populate({path: 'detalleHorario', select: 'description maxTime maxTimeDay -_id'})
        .select({ "_id":0,"id":0})
        .sort({idPedido : -1})
         .exec(function(error, resultado) {
             if (resultado.length<=0)
             {
             res.status(404).send({"error" : "Pedido no existe" });
             }
             else {
               res.status(201).send(resultado);
             }
           });
  };

//GET Pedido por Fecha
  function getOrderByDate(req,res){
      var date=req.params.date;
      order.find({"fechaRegistro":{$regex:"(?i).*"+date+".*"}})
      .populate({path: 'orderProds', select:'nombre descripcion nombreImg typeId precio -_id'})
      .populate({path: 'detalleHorario', select: 'description maxTime maxTimeDay -_id'})
      .select({ "_id":0,"id":0})
      .sort({idPedido : -1})
       .exec(function(error, resultado) {
           if (resultado.length<=0)
           {
           res.status(404).send({"error" : "Pedido no existe" });
           }
           else {
             res.status(201).send(resultado);
           }
         });
  };

  //GET Pedido por Usuario y Fecha
    function getOrderByUserDate(req,res){
        var id=req.params.idUser;
        var date=req.params.date;
        order.find({"idUsuario":+id,"fechaRegistro":{$regex:"(?i).*"+date+".*"}})
        .populate({path: 'orderProds', select:'nombre descripcion nombreImg typeId precio -_id'})
        .populate({path: 'detalleHorario', select: 'description maxTime maxTimeDay -_id'})
        .select({ "_id":0,"id":0})
        .sort({idPedido : -1})
         .exec(function(error, resultado) {
             if (resultado.length<=0)
             {
             res.status(404).send({"error" : "Pedido no existe" });
             }
             else {
               res.status(201).send(resultado);
             }
           });
      };

//INSERTAR pedido
 function createOrder(req, res){
         var queryString = 's={ idPedido: -1}&';
         httpClient.get('orders?' + queryString + apiKeyMLab,
           function(error, respuestaMLab, body){
                 var ulId = parseInt(body[0].idPedido);
                 newID = ulId + 1;
                 //validar si el producto array esta vacio
                 var arrProdPed = req.body.producto||[];
                  if (arrProdPed.length > 0) {
                    //si tiene productos para una orden procede a crear objeto
                    var newPedido = {
                         "idPedido"        : newID,
                         "producto"        : arrProdPed,
                         "idUsuario"       : req.body.idUsuario,
                         "montoTotal"      : req.body.montoTotal,
                         "fechaRegistro"   : req.body.fechaRegistro,
                         "fechaEntrega"    : req.body.fechaEntrega,
                         "fechaRecojo"     : req.body.fechaRecojo,
                         "fechaCancelado"  : req.body.fechaCancelado,
                         "horarioAsignado" : req.body.horarioAsignado,
                         "tipoPago"        : req.body.tipoPago,
                         "estado"          : "registrado"
                      };
                      // selecciona productos para descontar el stock
                      var queryProd = product.find({}).select({ "_id":0,"idProducto": 1,"nombre":1,"stock": 1});
                      queryProd.exec(function(error, products){
                          if(error){
                            //por si falla la consulta a los productos
                            res.send({"error":"Ha surgido un error al cargar productos"});}
                          else {
                            //hace match de los productos y los productos pedidos y
                            //actualiza el actualiza stock
                            //en caso de error hace rollback
                            liga = actualizarStockProducto(products, arrProdPed, res, product);
                            //si ha pasado bien la función anterior se crea un pedido
                            if(liga === "OK")
                            {
                                httpClient.post(baseMLabURL + "orders?" +  apiKeyMLab, newPedido,
                                function(error, respuestaMLab, body){
                                if(error)
                                {
                                  res.status(501).send({"error" : "error ingresando pedido"});
                                }
                                else {
                                  res.status(201).send({"msg" : "Pedido creado"});
                                    //Email
                                    var nombre, correo;
                                    var pedido = newPedido;
                                    var product = products;
                                    var idUsu=pedido.idUsuario

                                    var qStringID = 'q={"idUsuario":' + idUsu + '}&';
                                    httpClient.get('users?' + qStringID + qStringField + apiKeyMLab,
                                      function(err, respuestaMLab, body){
                                        var response = {};
                                        if (err) {
                                          res.status(500).res({"msg" : "Error obteniendo usuario"});
                                        }else {
                                          if (body.length > 0) {
                                            response = body;
                                            correo = body[0].correo;
                                            nombre = body[0].nombres;
                                            sendEmail(req,res,pedido,nombre,correo,product);
                                          }else {
                                            res.status(500).res({"msg" : "Error obteniendo usuario"});
                                          }
                                        }
                                      });

                                } });
                            }
                          }
                       })
                   }
                   else{
                     res.status(404).send({"error" : "No existe productos para el pedido"});
                   }
           });
};
//Funcion permite ordenar objetos de un arreglo en base a una propiedad
function dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
                var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}

//funcion para actualizar el stock del pedido
function actualizarStockProducto(arr1, arr2,res)
{
  var sProd    = arr1.sort(dynamicSort("idProducto"));
  var sProdPed = arr2.sort(dynamicSort("idProducto"));
  var x = 0;
  var j = 0;
  var l = 0;
  var nvoStok = 0;
  var prodRoll = [];
  var liga = "NOK";

  while (x < sProdPed.length && j < sProd.length)
  {
      if (sProdPed[x].idProducto === sProd[j].idProducto) {
          prodRoll.push(sProd[j]);
           if(sProdPed[x].cantidad <= sProd[j].stock)
           {
            //Calcular nuevo stock
               nvoStok =  sProd[j].stock - sProdPed[x].cantidad;

                 product.update(
                  { "idProducto": sProd[j].idProducto},
                  { "stock": nvoStok});
                liga = "OK";
           }
           else {
               while (l < prodRoll.length)
               {
                   product.update(
                   { "idProducto": prodRoll[l].idProducto},
                   { "stock": prodRoll[l].stock});
                 l++;
                }
                liga = "NOK";
                res.status(404).send({"error":"Producto sin stock "+sProd[j].nombre});
                break;
              }
          x++;
          j++;
      }
      else if(sProdPed[x].idProducto < sProd[j].idProducto)
      { x++;}
      else { j++;}
    }
    return liga;
}

//DELETE Pedido po ID
 function deleteOrderById(req, res){
   var id = req.params.id;
   var queryStringID = 'q={"idPedido":' + id + '}&';
   httpClient.get('orders?' +  queryStringID + apiKeyMLab,
     function(error, respuestaMLab, body){
       var respuesta = body[0];
       httpClient.delete(baseMLabURL + "orders/" + respuesta._id.$oid +'?'+ apiKeyMLab,
         function(error, respuestaMLab,body){
           res.status(200).send(body);
       });
     });
 };

//Actualiza estado con fechaEntrega
function updateOrderEstadoFecha(req, res){
  var id = Number(req.params.id);
  var estado = req.params.status;
  var fechaHoy = req.params.date;
  let queryString = 'q={"idPedido":' + id + '}&';

  if (estado === "entregado")
  {
    httpClient.get('orders?' + queryString + limFlt + apiKeyMLab,
      function(err,respuestaMLab,body){
        if (!err) {
          if (body.length == 1) {
            let modificado = '{"$set" :{"estado":"entregado","fechaEntrega":"'+fechaHoy+'"}}';

            httpClient.put('orders?q={"idPedido": ' + body[0].idPedido + '}&' + apiKeyMLab , JSON.parse(modificado),
              function (errPut,resPut,bodyPut){
                res.status(200).send({"msg":"Pedido modificado correctamente"});
            })
          } else {
            res.status(404).send({"error":"Pedido no encontrado"});
          }
        } else {
        res.status(501).send({"error":"no se pudo actualizar"});
        }
      }
    );
  }
  else {
    if (estado === "cancelado")
    {
      httpClient.get('orders?' + queryString + limFlt + apiKeyMLab,
        function(err,respuestaMLab,body){
          if (!err) {
            if (body.length == 1) {
              let modificado = '{"$set" :{"estado":"cancelado","fechaCancelado":"'+fechaHoy+'"}}';

              httpClient.put('orders?q={"idPedido": ' + body[0].idPedido + '}&' + apiKeyMLab , JSON.parse(modificado),
                function (errPut,resPut,bodyPut){
                  res.status(200).send({"msg":"Pedido modificado correctamente"});
              })
            } else {
              res.status(404).send({"error":"Pedido no encontrado"});
            }
          } else {
          res.status(501).send({"error":"no se pudo actualizar"});
          }
        }
      );
    }
    else {
      res.status(404).send({"error":"estado no permitido"});
    }
  }
};

//Email
//Se añade función para enviar la constancia de pedido por correo
function sendEmail(req, res, pedido,nombre1,correo,product){
      var horario,tipoPago;
      if (pedido.horarioAsignado == "H1") {
        var horario = "12:30 pm - 1:00 pm";
      }else {
        var horario = "1:00 pm - 1:30 pm";
      }

      if (pedido.tipoPago == "TC") {
        var tipoPago = "Tarjeta";
      }else {
        var tipoPago = "Lukita";
      }

          mensaje="Hola " + "<b>" + nombre1 + "</b>" + ", su pedido ha sido realizado.<br>";
          var detPed = "<ul><b>Número de Pedido: </b>"+pedido.idPedido+"</ul>";

              for(var x= 0; x<pedido.producto.length;x++ ){
                  var idProducto = pedido.producto[x].idProducto;
                  var nomProducto = product[idProducto].nombre;
                  detPed = detPed +"<ul><b>Producto: </b>"+nomProducto+"</ul>";
                  detPed = detPed +"<ul><b>Cantidad: </b>"+pedido.producto[x].cantidad+"</ul>";
                }

                  detPed = detPed + "<ul><b>Total : </b>"+pedido.montoTotal+" Soles</ul>"+
                  "<ul><b>Horario de entrega: </b>"+ horario+"</ul>"+
                  "<ul><b>Fecha de Compra: </b>"+ pedido.fechaRegistro+"</ul>"+
                  "<ul><b>Medio de pago: </b>"+tipoPago+"</ul>";

                  //QR
                  var qrPedido = pedido.idPedido + '\n';

                  for(var x= 0; x<pedido.producto.length;x++ ){
                      var idProducto = pedido.producto[x].idProducto;
                      qrPedido = qrPedido + product[idProducto].nombre + '\n';
                      qrPedido = qrPedido + pedido.producto[x].cantidad+ '\n';
                    }

                  qrPedido = qrPedido + pedido.montoTotal + '\n' + horario + '\n' + pedido.fechaRegistro + '\n' + tipoPago ;

                  QRCode.toDataURL(qrPedido, function (err, url) {
                    if (err) console.log(err);
                      var base64String = url;
                      let base64Image = base64String.split(';base64,').pop();
                      FS.writeFile('./imgs/QRPedido.png', base64Image, {encoding: 'base64'}, function(err) {
                        if (err) console.log(err);
                      })
                    });

                    mensaje = mensaje + detPed + "<br>";
                  var mailOptions = {
                        from: '"Comedor BBVA" <fastlunchbbva@gmail.com>',
                        to: correo,
                        subject: 'Confirmación de Pedido Comedor BBVA',
                        html: mensaje,
                        attachments: [
                             {
                              path: './imgs/QRPedido.png'
                             }
                          ]
                    }

                  transporter.sendMail(mailOptions, function(error, info){
                    if (error){
                      res.send(500, error);
                    }
                  });
};

//Export FUNCTIONS
module.exports.getOrder = getOrder;
module.exports.getOrderById = getOrderById;
module.exports.getOrderByUser = getOrderByUser;
module.exports.getOrderByDate = getOrderByDate;
module.exports.getOrderByUserDate = getOrderByUserDate;
module.exports.createOrder = createOrder;
module.exports.deleteOrderById = deleteOrderById;
module.exports.updateOrderEstadoFecha = updateOrderEstadoFecha;
