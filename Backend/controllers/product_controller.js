const apiKeyMLab="apiKey="+process.env.MLAB_API_KEY;
const baseMLabURL=process.env.BASEMLABURL;
var express = require('express');
const cors = require('cors');
let requestJSON = require('request-json');
var queryStringField = 'f={"_id":0}&';
var httpClient = requestJSON.createClient(baseMLabURL);

//GET todos los productos
function getProduct(req,res){
          httpClient.get('products?'+queryStringField+apiKeyMLab,
          function(err, respuestaMLab, body)
          {
            if(err){
              res.status(500).send({"msg":"error obteniendo producto."});
            }
            else {
                if (body.length>0) {
                  res.status(201).send(body);
                }
                else {
                  res.status(404).send({"msg":"ningun elemento producto."});
                }
            }
          });
  };

//GET producto por ID
function getProductById(req,res){
      var id=req.params.id;
      var queryString = 'q={"idProducto":'+id+'}&';
      httpClient.get('products?'+queryString+queryStringField+apiKeyMLab,
      function(err, respuestaMLab, body)
        {
          if(err){
              res.status(500).send({"msg":"error obteniendo producto."});
            }
          else {
                if (body.length>0) {
                    res.status(201).send(body);
                  }
                else {
                  res.status(404).send({"msg":"ningun elemento producto."});
                }
          }
      });
};

//GET Producto por tipo 1 O 2
function getProductByType(req,res){
    var typeId=req.params.typeId;
    var queryString = 'q={"typeId":'+typeId+'}&';
    httpClient.get('products?'+queryString+queryStringField+apiKeyMLab,
    function(err, respuestaMLab, body)
        {
          if(err){
              res.status(500).send({"msg":"error obteniendo producto por tipo."});
              }
          else {
                if (body.length>0) {
                    res.status(201).send(body);
                    }
                else {
                    res.status(404).send({"msg":"ningun elemento producto del tipo "+typeId+"."});
                      }
                }
        });
  };
//Crear Productos
 function createProduct(req, res){
         var queryString = 's={ idProducto: -1}&';
         httpClient.get('products?' + queryString + apiKeyMLab,
           function(error, respuestaMLab, body){
                 var ulId = parseInt(body[0].idProducto);
                 newID = ulId + 1;
                 var newProducto = {
                    "idProducto"  : newID,
                    "nombre"      : req.body.nombre,
                    "descripcion" : req.body.descripcion,
                        "stock"   : req.body.stock,
                     "nombreImg"  : req.body.nombreImg,
                     "typeId"     : req.body.typeId,
                     "indDia"     : req.body.indDia,
                     "precio"     : req.body.precio
                 };
                   httpClient.post(baseMLabURL + "products?" +  apiKeyMLab, newProducto,
                   function(error, respuestaMLab, body){
                     res.status(201).send(body);
                   });
           });
};

//ACTUALIZAR producto por ID
function updateProductById(req, res) {
     var id = req.params.id;
     var nid = parseInt(id);
     var queryString = 'q={"idProducto":' + id + '}&';
     httpClient.get('products?' + queryString + apiKeyMLab,
       function(err, respuestaMLab, body){
         let response = body[0];
         if (typeof response !== "undefined"){
             //Actualizo campos del producto
             let updatedProducto = {
               "idProducto"  : nid,
               "nombre"      : req.body.nombre,
               "descripcion" : req.body.descripcion,
                   "stock"   : req.body.stock,
                "nombreImg"  : req.body.nombreImg,
                "typeId"     : req.body.typeId,
                "indDia"     : req.body.indDia,
                "precio"     : req.body.precio
             };
             httpClient.put('products/' + response._id.$oid + '?' + apiKeyMLab, updatedProducto,
               function(err, respuestaMLab, body){
                 if(err) {
                       res.status(500).send({"msg" : "Error actualizando producto."});
                 } else {
                       res.status(200).send({"msg" : "producto actualizado correctamente."});
                 }
               });
           }
           else {
             res.status(500).send({"msg" : "Producto no existe para actualizar."});
           }
       });
 };

//DELETE producto por ID
 function deleteProductById(req, res){
   var id = req.params.id;
   var queryStringID = 'q={"idProducto":' + id + '}&';
   httpClient.get('products?' +  queryStringID + apiKeyMLab,
     function(error, respuestaMLab, body){
       var respuesta = body[0];
       if (typeof respuesta !== "undefined"){
           httpClient.delete(baseMLabURL + "products/" + respuesta._id.$oid +'?'+ apiKeyMLab,
             function(error, respuestaMLab,body){
                 res.status(200).send(body);
               });
           }
       else {
           res.status(500).send({"error": "no existe producto a eliminar."});
          }
        });
  };


module.exports.getProduct = getProduct;
module.exports.getProductById = getProductById;
module.exports.getProductByType = getProductByType;
module.exports.createProduct = createProduct;
module.exports.updateProductById = updateProductById;
module.exports.deleteProductById = deleteProductById;
